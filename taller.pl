taller(bob, mike).
taller(mike, jim).
taller(jim, george).

is_taller(X, Y):-
    taller(X, Z),
    is_taller(Z, Y).

is_taller(X, Y):-
    taller(X, Y).