move_to(town1, town2).
move_to(town2, town3).
move_to(town3, town4).
move_to(town4, town5).
move_to(town5, town6).

can_get(X, Y):-
    move_to(X, Z),
    can_get(Z, Y).

can_get(X, Y):-
    move_to(X, Y).