replace_all([], _, _, []).

replace_all([A|T], A, B, [B|Result]) :-
    replace_all(T, A, B, Result).

replace_all([H|T], A, B, [H|Result]) :- 
    H \= A,
    replace_all(T, A, B, Result).