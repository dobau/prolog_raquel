delete_all([], _, []).

delete_all([Letter|T], Letter, Result) :-
    delete_all(T, Letter, Result).

delete_all([H|T], Letter, [H|Result]) :-
    H \= Letter,
    delete_all(T, Letter, Result).